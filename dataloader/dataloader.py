import glob
import torch
from PIL import Image
import torch
import torch.utils.data as data
import pandas as pd


class FaceRecognitionDataset(data.Dataset):
    """Dataset for heartsegmentation in 3d"""

    def __init__(self, csv_file, img_path, multi_class=False, transform=None):
        """
        Args:
            csv_file(string): Path to csv file with all endo images
            mask_type(string): What to load, ENDO, EPI or MULTI files
            transform(callable, optional): Optional tranformation
        """
        self.csv_file = csv_file
        self.img_path = img_path
        self.transform = transform
        self.multi_class = multi_class
        self.df = pd.read_csv(self.csv_file, sep=';')

    def __len__(self):
        """Returns the number of data in the dataloader."""
        return len(self.df)

    def __getitem__(self, idx):
        """
        Gets the item by its index.
        Args:
            idx(int): Index of item to get."""

        path = self.img_path + self.df.at[int(idx), 'studyBranch'] + '/' + str(self.df.at[int(idx), 'image']) + '.jpg'

        scan = Image.open(path)
        
        if(self.transform):
            scan = self.transform(scan)
        """
           0 = Wirtschaft
           1 = Technik
           2 = Medien
           3 = Informatik"""
        categorys = {'AI':   [0],
                     'DI':   [0],
                     'UI':   [0],
                     'BWL':  [0],
                     'ECOM': [0],
                     'KAI':  [0],
                     'WING': [0],
                     'EA':   [1],
                     'PA':   [1],
                     'PI':   [1],
                     'II':   [1, 3],
                     'IAT':  [1, 3],
                     'ITAT': [1, 3],
                     'ITE':  [1, 3],
                     'STEC': [1, 3],
                     'TINF': [1, 3],
                     'IA':   [3],
                     'ITAS': [3],
                     'MS':   [3],
                     'INF':  [3],
                     'ITS':  [3],
                     'MI':   [3, 2],
                     'CGT':  [3, 2],
                     'IAM':  [3, 2],
                     'ITAM': [3, 2],
                     'MINF': [3, 2],
                     'ITAW': [3, 0],
                     'IMCA': [3, 0],
                     'IAW':  [3, 0],
                     'WI':   [3, 0],
                     'WINF': [3, 0]}

        category_name = self.df.at[int(idx), 'studyBranch']
        category_index = categorys[category_name]

        if(self.multi_class):
            category_index = category_index[:1]

        mask = torch.zeros(4)
        for index in category_index:
            mask[index] = 1

        mask = mask.type(torch.LongTensor)
        return (scan, mask)
