import torch
import torch.nn as nn
from torchvision import transforms
import torch.optim as optim

from dataloader.dataloader import FaceRecognitionDataset
from nnet.faceClassifier import FaceClassifier
from nnet.net import Net

def main():
    # Hyperparameter
    batch_size = 10
    epochs = 20
    validation_size = 0.5

    learning_rate = 0.01
    momentum = 0.99

    use_gpu_count = 0

    all_devices = list(range(0, torch.cuda.device_count()))

    # temporary solution, don't use more than three because of crashes
    all_devices = all_devices[:3]

    devices = all_devices[:use_gpu_count]

    #  Other Parameter
    multi_class = False  # set it to ENDO, EPI or MULTI
    dataset_path = 'data/'
    dataset_csv = 'data/meta.csv'

    data_transforms = transforms.Compose([transforms.RandomCrop(200, pad_if_needed=True), transforms.ToTensor()])
    dataset = FaceRecognitionDataset(dataset_csv, dataset_path, multi_class, data_transforms)

    # dataset sizes TODO: set for cropping before
    channels = 3

    unet = Net(channels, classes=4)
#    error_weights = [0.05, 0.95, 0.95]
#    criterion = nn.CrossEntropyLoss(weight=torch.FloatTensor(error_weights))
    criterion = nn.CrossEntropyLoss()
    # implement dataparallel when gpu should be used
    if(use_gpu_count > 0 and torch.cuda.device_count() > 0):
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        if torch.cuda.device_count() > 1:
            print("Let's use", len(devices), "GPUs!")
            unet = nn.DataParallel(unet, device_ids=devices)
            criterion = nn.DataParallel(criterion, device_ids=devices)

        unet.to(device)
        criterion.to(device)
    else:
        print("Let's use CPU only!")

    optimizer = optim.SGD(unet.parameters(), learning_rate, momentum)

    segmenter = FaceClassifier(unet, dataset)

    segmenter.train(epochs, criterion, optimizer, batch_size, validation_size)


if __name__ == "__main__":
    main()
