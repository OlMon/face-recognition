import torch
import torch.nn as nn


class convLayer(nn.Module):
    def __init__(self, in_channels, out_channels, kSize, stride, kPool, sPool):
        super(convLayer, self).__init__()

        self.conv = nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kSize)
        self.batchNorm = nn.BatchNorm2d(out_channels)
        self.relu = nn.ReLU()
        self.pool = nn.MaxPool2d(kPool, sPool)

    def forward(self, batch):
        batch = self.conv(batch)
        batch = self.batchNorm(batch)
        batch = self.relu(batch)
        batch = self.pool(batch)

        return batch


class linearLayer(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(linearLayer, self).__init__()

        self.linear = nn.Linear(in_channels, out_channels)
        self.relu = nn.ReLU()

    def forward(self, batch):
        batch = self.linear(batch)
        batch = self.relu(batch)

        return batch


class Net(nn.Module):
    """Class that puts all necessary steps for a unet together."""
    def __init__(self, channels, classes=2):
        super(Net, self).__init__()

        kSize = 3
        stride = 1
        pool_kSize = 2
        pool_stride = 2

        self.conv1 = convLayer(channels, 40, kSize=kSize, stride=stride, kPool=pool_kSize, sPool=pool_stride)
        self.conv2 = convLayer(40, 80, kSize=kSize, stride=stride, kPool=pool_kSize, sPool=pool_stride)

        self.lin1 = linearLayer(80, 120)
        self.lin2 = linearLayer(120, 80)
        self.output_mask = linearLayer(80, classes)

    def forward(self, batch):
        batch = self.conv1(batch)
        batch = self.conv2(batch)

        batch = batch.view(batch.size(0), -1, self.num_flat_features(batch))

        batch = self.lin1(batch)
        batch = self.lin2(batch)

        out = self.output_mask(batch)
        return out

    def num_flat_features(self, x):
        size = x.size()[1:]  # all dimensions except the batch dimension
        num_features = 1
        for s in size:
            num_features *= s
            return num_features
