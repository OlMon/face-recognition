import torch
import torch.nn as nn
from torchvision import transforms
from PIL import Image
import torchvision
import torch.utils.data as torchData
import numpy as np
from tensorboardX import SummaryWriter


class FaceClassifier:
    """Class that gets the net and implements the training and validation."""
    def __init__(self, net, dataset):
        self.net = net
        self.dataset = dataset

    def _prepare_dataset(self, batch_size=1, validation_split=0.2):
        """Creates the dataloaders and splits the dataset to a training set and validation set."""
        dataset_size = len(self.dataset)
        validation_size = int(np.floor(validation_split * dataset_size))
        train_size = dataset_size - validation_size

        train_dataset, validation_dataset = torchData.random_split(self.dataset, [train_size, validation_size])

        train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size)
        validation_loader = torch.utils.data.DataLoader(validation_dataset, batch_size=batch_size)

        return train_dataset, validation_dataset, train_loader, validation_loader

    def _calc_epoch(self, train_loader, criterion, optimizer):
        """Function to calculate one whole epoch."""
        batch_loss = 0.0
        epoch_loss = 0.0
        self.net.train()
        for idx, (mini_batch, target) in enumerate(train_loader):
            optimizer.zero_grad()

            outputs = self.net.forward(mini_batch)
            
            loss = criterion(outputs, target)  # remove second dimension for cross entropy loss
            loss.sum().backward()  # need to do a sum, because of the many losses from the multiple gpu
            #loss.backward()
            optimizer.step()

            batch_loss = loss.sum().item()  # need to do a sum, because of the many losses from the multiple gpu
            epoch_loss += batch_loss
            if(idx % 10 == 9):
                print('[Batch %d] loss: %.5f' % (idx + 1, batch_loss))

        return (epoch_loss / len(train_loader.dataset))

    def train(self, epochs, criterion, optimizer, batch_size=1, validation_split=0.2):
        """Function to train the model.
           Args:
               epochs(int): How many epoch should be trained.
               optimizer(pytorch.optimizer): Optimizer to use during training.
               batch_size(int): What batch size to use during training.
               validation_split(float): How much of the dataset use for validation."""
        self.writer = SummaryWriter()
        self.train_dataset, self.validation_dataset, self.train_loader, self.validation_loader = self._prepare_dataset(batch_size, validation_split)

        self.writer.add_graph(self.net, torch.rand(1,3,200,200))

        for epoch in range(epochs):
            print('Starting %d. Epoch' % (epoch + 1))
            train_loss = self._calc_epoch(self.train_loader, criterion, optimizer)
            # After calculating an epoch, validate for taking statistics
            validation_loss = self.validate(self.validation_loader, criterion, epoch)
            self.writer.add_scalars("data/loss", {"training_loss": train_loss, "validation_loss": validation_loss}, epoch)
            print('Calculated epoch %d from %d' % (epoch, epochs))
        # export scalar data to JSON for external processing
        self.writer.export_scalars_to_json("./all_scalars.json")
        self.writer.close()
        print('Finished Training')

    def validate(self, validation_loader, criterion, epoch):
        """Function to validate the net.
           Args:
               criterion(method): Function to calculate the loss."""
        self.net.eval()
        loss = 0.0
        create_image = False
        with torch.no_grad():
            for (scan, target) in validation_loader:
                output = self.net.forward(scan)
                loss += criterion(output, target)
                # save images in the writer but only first image of first batch
                if(create_image):
                    pred_mask = output.argmax(dim=1, keepdim=True)
                    img = torch.cat((pred_mask, target.type(torch.LongTensor)), 2)
                    img = img[0].permute(1,0,2,3)
                    img_row = torchvision.utils.make_grid(img, nrow=10)
                    self.writer.add_image("masks/conc", img_row, epoch)
                    create_image = False

        # return (loss.sum().item() / len(validation_loader.dataset))
        return (loss / len(validation_loader.dataset))
